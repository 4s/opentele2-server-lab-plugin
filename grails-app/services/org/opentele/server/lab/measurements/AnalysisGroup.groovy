package org.opentele.server.lab.measurements

class AnalysisGroup {

    // --*-- Fields --*--

    String analysisType
    String analysisCodeType
    String analysisTypeMetadata
    String unit
    List<LabMeasurement> measurements

    // --*-- Constructors --*--

    AnalysisGroup(String analysisType, String analysisCodeType, String analysisTypeMetadata,
                  String unit, List<LabMeasurement> measurements) {
        this.analysisType = analysisType
        this.analysisCodeType = analysisCodeType
        this.analysisTypeMetadata = analysisTypeMetadata
        this.unit = unit
        this.measurements = measurements
    }

    // --*-- Methods --*--

    String getAnalysisType() { analysisType }

    String getAnalysisCodeType() { analysisCodeType }

    String getAnalysisTypeMetadata() { analysisTypeMetadata }

    String unit() { unit }

    List<LabMeasurement> measurements() { measurements }

}
