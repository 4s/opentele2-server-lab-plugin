package org.opentele.server.lab.measurements

class LabMeasurement {

    // --*-- Fields --*--

    boolean hasBeenCorrected
    boolean hasComments
    int order
    String value
    String valueMetadata
    String analysisType
    String analysisCodeType
    String analysisTypeMetadata
    String unit
    String requisitionGroup
    Date time
    Indicator indicator
    Validation validation

    // --*-- Constructors --*--

    LabMeasurement(String value, String valueMetadata, String analysisType, String analysisCodeType,
                   String analysisTypeMetadata, String unit, Date time, Indicator indicator,
                   String requisitionGroup, Validation validation, int order, boolean hasBeenCorrected,
                   boolean hasComments) {
        this.value = value
        this.valueMetadata = valueMetadata
        this.analysisType = analysisType
        this.analysisCodeType = analysisCodeType
        this.analysisTypeMetadata = analysisTypeMetadata
        this.unit = unit
        this.time = time
        this.indicator = indicator
        this.requisitionGroup = requisitionGroup
        this.validation = validation
        this.order = order
        this.hasBeenCorrected = hasBeenCorrected
        this.hasComments = hasComments
    }

    // --*-- Methods --*--

    String value() { value }

    String valueMetadata() { valueMetadata }

    String analysisType() { analysisType }

    String analysisCode() { analysisCodeType }

    String analysisTypeMetadata() { analysisTypeMetadata }

    Date time() { time }

    String unit() { unit }

    Indicator indicator() { indicator }

    String requisitionGroup() { requisitionGroup }

    Validation validation() { validation }

    int order() { order }

    boolean hasBeenCorrected() { hasBeenCorrected }

    boolean hasComments() { hasComments }

}
