package org.opentele.server.lab.measurements

class MeasurementGroup {

    // --*-- Fields --*--

    String type
    List<AnalysisGroup> analysisGroups

    // --*-- Constructors --*--

    MeasurementGroup(String type, List<AnalysisGroup> analysisGroups) {
        this.type = type
        this.analysisGroups = analysisGroups
    }

    // --*-- Methods --*--

    String type() { type }

    List<AnalysisGroup> analysisGroups() { analysisGroups }

}
