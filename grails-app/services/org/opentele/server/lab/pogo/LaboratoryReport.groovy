package org.opentele.server.lab.pogo

import groovy.transform.ToString

@ToString
class LaboratoryReport {

    Letter letter = new Letter()
    Sender sender = new Sender()
    Receiver receiver = new Receiver()
    CCReceiver ccReceiver
    LabPatient patient = new LabPatient()
    RequisitionInformation requisitionInformation = new RequisitionInformation()

    LaboratoryResults laboratoryResults = new LaboratoryResults()

}
