package org.opentele.server.lab.pogo

import groovy.transform.ToString

@ToString
class Receiver {

    def eanIdentifier
    def identifier
    def identifierCodeOrLocal

    def organisationName
    def departmentName
    def unitName
    def streetName
    def suburbName
    def districtName
    def postCodeIdentifier
    def physicianInitials

}
