package org.opentele.server.lab.pogo

import groovy.transform.ToString

@ToString
class RequisitionGroup {

    def identifier
    def identifierResponsible
    def name
}
