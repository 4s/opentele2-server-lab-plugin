package org.opentele.server.lab.pogo

import groovy.transform.ToString

@ToString
class Emessage {

    // Envelope
    String sentDate
    String sentTime
    String acknowledgementCode
    String identifier

    //LaboratoryReports
    List<LaboratoryReport> laboratoryReports = new ArrayList<LaboratoryReport>()
}
