package org.opentele.server.lab

import grails.test.mixin.TestFor
import org.opentele.server.lab.pogo.Emessage
import org.opentele.server.lab.pogo.LabPatient
import org.opentele.server.lab.pogo.Letter
import org.opentele.server.lab.pogo.Receiver
import org.opentele.server.lab.pogo.RequisitionInformation
import org.opentele.server.lab.pogo.Result
import org.opentele.server.lab.pogo.Sender
import spock.lang.Specification

@TestFor(Xrpt01ParserService)
class Xrpt01ParserSpec extends Specification {

    private static final String PATH_PREFIX =
            'test/unit/org/opentele/server/lab/test_files/'
    private static final String UTF_8 = "UTF-8"

    private static final String MEDCOM_NAMESPACE =
            'http://rep.oio.dk/sundcom.dk/medcom.dk/xml/schemas/2007/02/01/'

    private static final String XRPT01_EXAMPLE1_XML =
            new File(PATH_PREFIX + "XRPT01Example.xml").getText(UTF_8)
    private static final String XRPT01_EXAMPLE2_XML =
            new File(PATH_PREFIX + "XRPT02Example.xml").getText(UTF_8)

    def setup() {
        //service.log = Mock(Log) // Just to avoid excessive output during test
    }

    def 'can read xrpt01 envelope'() {
        when:
        def result = service.parse(XRPT01_EXAMPLE1_XML, [])

        then:
        result.sentDate == "2000-01-14"
        result.sentTime == "12:47"
        result.identifier == "00114124637000"
        result.acknowledgementCode == "minuspositivkvitt"
    }

    def 'can read xrpt01 laboratory report header'() {
        when:
        def result = service.parse(XRPT01_EXAMPLE1_XML, [])

        then:
        result.sentDate == "2000-01-14"
        result.sentTime == "12:47"
        result.identifier == "00114124637000"
        result.acknowledgementCode == "minuspositivkvitt"
    }

    def 'can read xrpt01 Letter'() {
        when:
        Emessage result = service.parse(XRPT01_EXAMPLE1_XML, [])

        then:
        result.laboratoryReports[0].letter.identifier == "80901082504854"
        result.laboratoryReports[0].letter.versionCode == "XR0130K"
        result.laboratoryReports[0].letter.statisticalCode == "XRPT01"
        result.laboratoryReports[0].letter.authorisationDate == "2000-01-14"
        result.laboratoryReports[0].letter.authorisationTime == "12:47"
        result.laboratoryReports[0].letter.typeCode == "XRPT01"
    }

    def 'can read xrpt01 Sender'() {
        when:
        Emessage result = service.parse(XRPT01_EXAMPLE1_XML, [])

        then:
        result.laboratoryReports[0].sender.eanIdentifier == "5790000188420"
        result.laboratoryReports[0].sender.identifier == "2000830"
        result.laboratoryReports[0].sender.identifierCodeOrLocal == "sygehusafdelingsnummer"
        result.laboratoryReports[0].sender.organisationName == "Hillerød Sygehus"
        result.laboratoryReports[0].sender.departmentName == "Klinisk Kemisk Afdeling"
        result.laboratoryReports[0].sender.medicalSpecialityCode == "klin_biokemi"
        result.laboratoryReports[0].sender.fromLabIdentifier == "lab x"
    }

    def 'can read xrpt01 Receiver'() {
        when:
        Emessage result = service.parse(XRPT01_EXAMPLE1_XML, [])

        then:
        result.laboratoryReports[0].receiver.eanIdentifier == "5790000125012"
        result.laboratoryReports[0].receiver.identifier == "989897"
        result.laboratoryReports[0].receiver.identifierCodeOrLocal == "ydernummer"
        result.laboratoryReports[0].receiver.organisationName == "Lægehuset"
        result.laboratoryReports[0].receiver.departmentName == "Lægerne"
        result.laboratoryReports[0].receiver.streetName == "Midttværvej 33"
        result.laboratoryReports[0].receiver.districtName == "Hillerød"
        result.laboratoryReports[0].receiver.postCodeIdentifier == "3400"
        result.laboratoryReports[0].receiver.physicianInitials == "FK"
    }

    def 'can read xrpt01 Patient'() {
        when:
        Emessage result = service.parse(XRPT01_EXAMPLE1_XML, [])

        then:
        result.laboratoryReports[0].patient.cpr == "0108624884"
        result.laboratoryReports[0].patient.surname == "Svendsen"
        result.laboratoryReports[0].patient.givenName == "Jane"
        result.laboratoryReports[0].patient.consentGiven == "true"

    }

    def 'can read xrpt01 RequisitionInformation'() {
        when:
        Emessage result = service.parse(XRPT01_EXAMPLE1_XML, [])

        then:
        result.laboratoryReports[0].requisitionInformation.comments == "Prøverne var mere end 24 timer undervejs." // <Break/>
        result.laboratoryReports[0].requisitionInformation.laboratoryInternalSampleIdentifier == "00875137"
        result.laboratoryReports[0].requisitionInformation.requesterSampleIdentifier == "00875137"
        result.laboratoryReports[0].requisitionInformation.samplingDate == "1998-08-27"
        result.laboratoryReports[0].requisitionInformation.samplingTime == "06:30"
    }

    def 'can extract GeneralResultInfo'() {
        when:
        Emessage result = service.parse(XRPT01_EXAMPLE1_XML, [])

        then:
        result.laboratoryReports[0].laboratoryResults.generalResultInformation.laboratoryInternalProductionIdentifier == "0000426825"
        result.laboratoryReports[0].laboratoryResults.generalResultInformation.reportStatusCode == "svar_endeligt"
        result.laboratoryReports[0].laboratoryResults.generalResultInformation.resultsDate == "2000-01-14"
        result.laboratoryReports[0].laboratoryResults.generalResultInformation.resultsTime == "12:47"
    }


    def 'can handle no physician in result'() {
        given:
        def xml = new File(PATH_PREFIX + 'XRPT01ExampleNoPhysician.xml').getText(UTF_8)

        when:
        Emessage result = service.parse(xml, [])

        then:
        result.laboratoryReports[0].receiver.physicianInitials == null
    }

    def 'can extract Results'() {
        when:
        Emessage result = service.parse(XRPT01_EXAMPLE2_XML, ['NPU02319','NPU01944','NPU03404','NPU03568',
                                                              'NPU02593','NPU03577','NPU03624','NPU03578',
                                                              'NPU01807','NPU01566','NPU01423'])
        then:

        result.laboratoryReports[0].laboratoryResults.results.size() == 11

        result.laboratoryReports[0].laboratoryResults.results.each {
            println it
        }
    }

    def 'can parse result'() {

        def xml = """<Result>
        <ResultStatusCode>svar_endeligt</ResultStatusCode>
        <Analysis>
          <AnalysisCode>NPU03568</AnalysisCode>
          <AnalysisCodeType>iupac</AnalysisCodeType>
          <AnalysisCodeResponsible>SST</AnalysisCodeResponsible>
          <AnalysisShortName>Thrombocytter;B</AnalysisShortName>
          <AnalysisCompleteName>B-Thrombocytter, antalk.<Break/></AnalysisCompleteName>
        </Analysis>
        <ProducerOfLabResult>
          <Identifier>4202120 KKA Odense UNI.Hospital</Identifier>
          <IdentifierCode>OUH</IdentifierCode>
        </ProducerOfLabResult>
        <ReferenceInterval>
          <TypeOfInterval>uspecificeret</TypeOfInterval>
          <LowerLimit>150</LowerLimit>
          <UpperLimit>400</UpperLimit>
        </ReferenceInterval>
        <ResultType>numerisk</ResultType>
        <Value>100</Value>
        <Unit>10E9/l</Unit>
        <ResultValidation>for_lav</ResultValidation>
        <ToLabIdentifier>test</ToLabIdentifier>
      </Result>"""

        when:
        service.npuCodes = ['NPU03568']
        def resNode = new XmlSlurper().parseText(xml).declareNamespace([meta:MEDCOM_NAMESPACE])
        Result result = service.parseResult(resNode)

        then:
        result.resultType == "numerisk"
        result.value == "100"
        result.unit == "10E9/l"
        result.resultValidation == "for_lav"
        result.analysis.analysisCode == "NPU03568"
        result.references.size() == 0
        result.analysis.requisitionGroup == null
        result.producerOfLabResult.identifier == "4202120 KKA Odense UNI.Hospital"
        result.producerOfLabResult.identifierCode == "OUH"
        result.toLabIdentifier == "test"

    }

    def 'can parse Sender'() {

        def xml = """<Sender>
    <EANIdentifier>5790000120284</EANIdentifier>
      <Identifier>4201050</Identifier>
    <IdentifierCode>sygehusafdelingsnummer</IdentifierCode>
      <OrganisationName>Sygehus Fyn, Svendborg</OrganisationName>
    <DepartmentName>Klinisk Kemisk Afdeling</DepartmentName>
      <MedicalSpecialityCode>klin_biokemi</MedicalSpecialityCode>
    </Sender>"""

        when:
        def resNode = new XmlSlurper().parseText(xml).declareNamespace([meta:MEDCOM_NAMESPACE])
        Sender result = new Sender()

        service.parseSender(resNode, result)

        then:
        result.eanIdentifier == "5790000120284"
        result.identifier == "4201050"
        result.identifierCodeOrLocal == "sygehusafdelingsnummer"
        result.organisationName == "Sygehus Fyn, Svendborg"
        result.departmentName == "Klinisk Kemisk Afdeling"
        result.medicalSpecialityCode == "klin_biokemi"
    }

    def 'can parse Letter'() {

        def xml = """<Letter>
    <Identifier>80901082504854</Identifier>
      <VersionCode>XR0130K</VersionCode>
    <StatisticalCode>XRPT01</StatisticalCode>
      <Authorisation>
        <Date>2000-01-14</Date>
    <Time>12:47</Time>
      </Authorisation>
    <TypeCode>XRPT01</TypeCode>
    </Letter>"""

        when:
        def resNode = new XmlSlurper().parseText(xml)
                .declareNamespace([meta:MEDCOM_NAMESPACE])
        Letter result = new Letter()

        service.parseLetter(resNode, result)

        then:
        result.identifier == "80901082504854"
        result.versionCode == "XR0130K"
        result.statisticalCode == "XRPT01"
        result.authorisationDate == "2000-01-14"
        result.authorisationTime == "12:47"
        result.typeCode == "XRPT01"
    }

    def 'can parse Receiver'() {

        def xml = """<Receiver>
      <EANIdentifier>5790000125012</EANIdentifier>
    <Identifier>989897</Identifier>
      <IdentifierCode>ydernummer</IdentifierCode>
    <OrganisationName>Finn Klamer</OrganisationName>
      <DepartmentName>Læge</DepartmentName>
    <StreetName>Lægehuset</StreetName>
      <DistrictName>Erslev</DistrictName>
    <PostCodeIdentifier>5777</PostCodeIdentifier>
      <Physician>
        <PersonInitials>FK</PersonInitials>
    </Physician>
    </Receiver>"""

        when:
        def resNode = new XmlSlurper().parseText(xml).declareNamespace([meta:MEDCOM_NAMESPACE])
        Receiver result = new Receiver()

        service.parseReceiver(resNode, result)

        then:
        result.eanIdentifier == "5790000125012"
        result.identifier == "989897"
        result.identifierCodeOrLocal == "ydernummer"
        result.organisationName == "Finn Klamer"
        result.departmentName == "Læge"
        result.streetName == "Lægehuset"
        result.districtName == "Erslev"
        result.postCodeIdentifier == "5777"
        result.physicianInitials == "FK"

    }

    def 'can parse Patient'() {

        def xml = """<Patient>
    <CivilRegistrationNumber>1212778221</CivilRegistrationNumber>
      <PersonSurnameName>Mortensen</PersonSurnameName>
    <PersonGivenName>Jens</PersonGivenName>
    </Patient>"""

        when:
        def resNode = new XmlSlurper().parseText(xml).declareNamespace([meta:MEDCOM_NAMESPACE])
        LabPatient result = new LabPatient()

        service.parsePatient(resNode, result)

        then:
        result.cpr == "1212778221"
        result.givenName == "Jens"
        result.surname == "Mortensen"
    }

    def 'can parse RequisitionInformation'() {

        def xml = """<RequisitionInformation>
    <Comments>Prøverne var mere end 24 timer undervejs.<Break/></Comments>
      <Sample>
        <LaboratoryInternalSampleIdentifier>00875137</LaboratoryInternalSampleIdentifier>
    <RequesterSampleIdentifier>00875137</RequesterSampleIdentifier>
        <SamplingDateTime>
          <Date>1998-08-27</Date>
    <Time>06:30</Time>
        </SamplingDateTime>
    </Sample>
    </RequisitionInformation>"""

        when:
        def resNode = new XmlSlurper().parseText(xml).declareNamespace([meta:MEDCOM_NAMESPACE])
        RequisitionInformation result = new RequisitionInformation()

        service.parseRequisitionInformation(resNode, result)

        then:
        result.comments == "Prøverne var mere end 24 timer undervejs."
        result.laboratoryInternalSampleIdentifier == "00875137"
        result.requesterSampleIdentifier == "00875137"
        result.samplingDate == "1998-08-27"
        result.samplingTime == "06:30"
        result.samplingIntervalStartTime == null
        result.samplingIntervalEndTime == null
        result.volume == null
        result.unitOfVolume == null
    }

    def 'can parse RequisitionInformation w/SamplingInterval'() {

        def xml = """<RequisitionInformation>
    <Comments></Comments>
      <Sample>
        <LaboratoryInternalSampleIdentifier></LaboratoryInternalSampleIdentifier>
        <RequesterSampleIdentifier></RequesterSampleIdentifier>
        <SamplingDateTime>
          <Date></Date>
          <Time></Time>
        </SamplingDateTime>
        <SamplingInterval>
      <StartDateTime><Date>1998-08-27</Date><Time>09:56</Time>
      </StartDateTime>
      <EndDateTime><Date>1999-09-27</Date><Time>08:45</Time>
      </EndDateTime>
    </SamplingInterval>
    </Sample>
    </RequisitionInformation>"""

        when:
        def resNode = new XmlSlurper().parseText(xml).declareNamespace([meta:MEDCOM_NAMESPACE])
        RequisitionInformation result = new RequisitionInformation()

        service.parseRequisitionInformation(resNode, result)

        then:
        result.samplingIntervalStartDate == "1998-08-27"
        result.samplingIntervalStartTime == "09:56"
        result.samplingIntervalEndDate == "1999-09-27"
        result.samplingIntervalEndTime == "08:45"

        result.unitOfVolume == null
    }

}
